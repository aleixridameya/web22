#! /bin/bash


cp /opt/docker/daytime /etc/xinetd.d/
cp /opt/docker/echo  /etc/xinetd.d/
cp /opt/docker/chargen /etc/xinetd.d/

xinetd -dontfork
service xinetd status

